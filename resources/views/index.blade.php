<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>Web Page</title>
</head>
<body>
    <header id="main-header" class="fixed-header">
        <div class="navbar">
            <div class="logo">
                <img src="{{ asset('img/logo_suit(1).png') }}" alt="Logo">
            </div>
            <nav>
                <ul>
                    <li><a href="#">Work</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="{{ route('ideas') }}">Ideas</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <section id="banner">
        <div class="banner-content">
            <h1>Ideas</h1>
            <p>Where all our great things begin</p>
        </div>
        <img src="{{ asset('img/banner-img(1).jpg') }}" alt="Banner Image">
    </section>

    <section id="post-list">
        <div class="controls">
            <label for="sort">Sort By:</label>
            <select id="sort">
                @for ($i = 0; $i < 2; $i++)
                    <option value="{{ ($i == 0) ? 'published_at' : '-published_at' }}">
                        {{ ($i == 0) ? 'Newest' : 'Oldest' }}
                    </option>
                @endfor
            </select>

            <label for="show-per-page">Show Per Page:</label>
            <select id="show-per-page">
                @for ($perPage = 10; $perPage <= 50; $perPage += 10)
                    <option value="{{ $perPage }}">{{ $perPage }}</option>
                @endfor
            </select>
        </div>

        <div id="posts-container"></div>
        <h3></h3>
        <p></p>
    </section>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</body>
</html>

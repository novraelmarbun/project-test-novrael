<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        try {
            // Panggil metode fetchIdeas untuk mendapatkan data dari API
            $ideas = $this->fetchIdeas($request);

            // Tampilkan data ke view
            return view('welcome', ['ideas' => $ideas]);
        } catch (\Exception $e) {
            // Tanggapi kesalahan umum
            \Log::error('Exception: ' . $e->getMessage());
            return view('welcome', ['ideas' => $ideas]); // Tampilkan halaman kosong atau pesan kesalahan
        }
    }

    public function fetchIdeas(Request $request)
    {
        $page = $request->input('page.number', 1);  
        $pageSize = $request->input('page.size', 10);
        $append = $request->input('append', ['small_image', 'medium_image']);
        $sort = $request->input('sort', 'published_at');

        $response = Http::withOptions(['verify' => false])
            ->withHeaders(['Accept' => 'application/json'])
            ->get('https://suitmedia-backend.suitdev.com/api/ideas', [
                'page[number]' => $page,
                'page[size]' => $pageSize,
                'append' => implode(',', $append),
                'sort' => $sort,
            ]);

        // Decode the JSON response content to an array
        $ideas = $response->json();

        return $ideas; // Return the array, not a JSON response
    }
}
